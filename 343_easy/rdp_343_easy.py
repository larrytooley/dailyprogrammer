'''
https://www.reddit.com/r/dailyprogrammer/comments/7hhyin/20171204_challenge_343_easy_major_scales/
'''

def note(scale, soflege):
    '''Takes a scale and solfege name of a note and returns the note'''
    notes = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
    soflege_intervals = {'Do': 0, 'Re': 2, 'Mi': 4, 'Fa': 5,
                         'So': 7, 'La': 9, 'Ti': 11}

    return notes[(notes.index(scale) + soflege_intervals[soflege]) % 12]
