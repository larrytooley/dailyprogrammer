import unittest
import rdp_343_easy

class TestRDP(unittest.TestCase):

    def test_note(self):
        self.assertEqual(rdp_343_easy.note("C", "Do"), "C")
        self.assertEqual(rdp_343_easy.note("C", "Re"), "D")
        self.assertEqual(rdp_343_easy.note("C", "Mi"), "E")
        self.assertEqual(rdp_343_easy.note("D", "Mi"), "F#")
        self.assertEqual(rdp_343_easy.note("A#", "Fa"), "D#")

if __name__ == '__main__':
    unittest.main()
